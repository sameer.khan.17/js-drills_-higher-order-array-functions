function filter(elements,cb)
{
    if(elements==undefined || elements.length==0 || cb==undefined)
    {
        return ([]);
    }
    else
    {    
        let newarr=[];
        for(let i=0;i<elements.length;i++)
        {
            if(cb(elements[i]))
            {
                newarr.push(elements[i]);
            }
        }
        return newarr;
    }

}
module.exports=filter;