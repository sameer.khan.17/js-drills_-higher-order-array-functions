const newarr=[];
function flatten(elements,length=0)
{
    
    if(elements==undefined )
    {
        return [];
    }
    else
    {    
        if(elements.length==length)
       {
           return [];
       }
        if(Array.isArray(elements[length]))
       {
            flatten(elements[length]);
       }
       else
       {
            newarr.push(elements[length]);
       }
       flatten(elements,length+1);
       return newarr;
    }
}

module.exports=flatten;