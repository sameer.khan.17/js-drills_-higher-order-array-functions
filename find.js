
function find(elements,cb)
{
    if(elements==undefined || elements.length==0 || cb==undefined)
    {
        return ([]);
    }
    else
    {    
       for(let i=0;i<elements.length;i++)
       {
           if(cb(elements[i]))
           {
               return elements[i];
           }
       }
       return undefined;
    }
}

module.exports=find;