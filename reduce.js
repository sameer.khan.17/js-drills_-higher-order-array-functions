function reduce(elements, cb, startingValue) {
    if (elements == undefined || elements.length == 0 || cb == undefined) {
        return ([]);
    }
    else {
        let index = 0;
        if (startingValue == undefined) {
            startingValue = elements[0];
            index = 1;
        }

        for (; index < elements.length; index++) {
            startingValue = cb(startingValue, elements[index], index, elements);
        }
        return startingValue;


    }

}
module.exports = reduce;