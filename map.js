
function map(elements,cb)
{
    if(elements==undefined || elements.length==0 || cb ==undefined)
    {
        return ([]);
    }
    else
    {    
        let newarr=[];
        for(let i=0;i<elements.length;i++)
        {
            newarr.push(cb(elements[i],i,elements));
        }
        return newarr;
    }

}
module.exports=map;
